package org.bitbucket.fredgrott.gwsrobogherk;

import android.app.Instrumentation;

import com.jayway.android.robotium.solo.Solo;

// TODO: Auto-generated Javadoc
/**
 * The Class StepDefinitions.
 */
public abstract class StepDefinitions {
    
    /** The instrumentation. */
    protected Instrumentation instrumentation;
    
    /** The solo driver. */
    protected Solo soloDriver;

    /**
     * Sets the test dependecies.
     *
     * @param instrumentation the instrumentation
     * @param soloDriver the solo driver
     */
    public void setTestDependecies(Instrumentation instrumentation, Solo soloDriver) {
        this.instrumentation = instrumentation;
        this.soloDriver = soloDriver;
    }

    /**
     * Sets the up scenario.
     *
     * @throws Exception the exception
     */
    public void setUpScenario() throws Exception {
    }
    
    /**
     * Clean up scenario.
     *
     * @throws Exception the exception
     */
    public void cleanUpScenario() throws Exception {
    }

    /**
     * For class.
     *
     * @param klass the klass
     * @return the step definitions
     * @throws RoboGherkException the robo gherk exception
     */
    static StepDefinitions forClass(Class<? extends Feature> klass) throws RoboGherkException {
        return new StepFinder(new RealStepClassLoader()).findStepsFor(klass);
    }
}