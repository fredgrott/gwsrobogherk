package org.bitbucket.fredgrott.gwsrobogherk;

// TODO: Auto-generated Javadoc
/**
 * The Class RoboGherkException.
 */
public class RoboGherkException extends AssertionError {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 743458777778964419L;

    /**
     * Instantiates a new robo gherk exception.
     */
    public RoboGherkException() {
    }

    /**
     * Instantiates a new robo gherk exception.
     *
     * @param throwable the throwable
     */
    public RoboGherkException(Throwable throwable) {
        super(throwable);
    }
}