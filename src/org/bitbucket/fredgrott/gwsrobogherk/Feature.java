package org.bitbucket.fredgrott.gwsrobogherk;

import android.app.Activity;
import android.test.ActivityInstrumentationTestCase2;

// TODO: Auto-generated Javadoc
/**
 * The Class Feature.
 */
public abstract class Feature extends ActivityInstrumentationTestCase2<Activity> {

    /** The environment. */
    private ScenarioEnvironment environment;

    /**
     * Instantiates a new feature.
     *
     * @param activityClass the activity class
     */
    @SuppressWarnings("unchecked")
    public Feature(Class<? extends Activity> activityClass) {
        super((Class<Activity>) activityClass);
    }
    
    /**
     * Sets the up.
     *
     * @throws Exception the exception
     * @see android.test.ActivityInstrumentationTestCase2#setUp()
     */
    @Override
    protected void setUp() throws Exception {
        super.setUp();
        environment = ScenarioEnvironment.buildEnvironment(getClass(), getInstrumentation());
    }

    /**
     * Tear down.
     *
     * @throws Exception the exception
     * @see android.test.ActivityInstrumentationTestCase2#tearDown()
     */
    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
        environment.tearDown();
    }

    /**
     * Given.
     *
     * @param stepName the step name
     */
    public void Given(String stepName) {
        call(stepName);
    }

    /**
     * When.
     *
     * @param stepName the step name
     */
    public void When(String stepName) {
        call(stepName);
    }

    /**
     * Then.
     *
     * @param stepName the step name
     */
    public void Then(String stepName) {
        call(stepName);
    }

    /**
     * And.
     *
     * @param stepName the step name
     */
    public void And(String stepName) {
        call(stepName);
    }

    /**
     * Call.
     *
     * @param action the action
     */
    private void call(String action) {
        prepareToExecute();
        environment.executeStepDefinition(action);
    }

    /**
     * Prepare to execute.
     */
    private void prepareToExecute() {
        getActivity();
    }
}