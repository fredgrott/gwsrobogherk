package org.bitbucket.fredgrott.gwsrobogherk;

import android.app.Instrumentation;

import com.jayway.android.robotium.solo.Solo;

// TODO: Auto-generated Javadoc
/**
 * The Class ScenarioEnvironment.
 */
public class ScenarioEnvironment {
    
    /** The solo. */
    private Solo solo;
    
    /** The step executor. */
    private StepExecutor stepExecutor;

    /**
     * Instantiates a new scenario environment.
     *
     * @param stepExecutor the step executor
     * @param solo the solo
     */
    public ScenarioEnvironment(StepExecutor stepExecutor, Solo solo) {
        this.solo = solo;
        this.stepExecutor = stepExecutor;
    }

    /**
     * Tear down.
     */
    void tearDown() {
        solo.finishOpenedActivities();
        stepExecutor.tearDown();
    }

    /**
     * Execute step definition.
     *
     * @param action the action
     */
    void executeStepDefinition(String action) {
        stepExecutor.call(action);
    }

    /**
     * Builds the environment.
     *
     * @param featureClass the feature class
     * @param instrumentation the instrumentation
     * @return the scenario environment
     * @throws RoboGherkException the robo gherk exception
     */
    static ScenarioEnvironment buildEnvironment(Class<? extends Feature> featureClass, Instrumentation instrumentation) throws RoboGherkException {
        StepDefinitions stepDefinitions = StepDefinitions.forClass(featureClass);
        StepExecutor stepExecutor = new StepExecutor(stepDefinitions);
        Solo solo = new Solo(instrumentation);
        stepExecutor.setUp(instrumentation, solo);
        return new ScenarioEnvironment(stepExecutor, solo);
    }
}