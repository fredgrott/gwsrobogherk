package org.bitbucket.fredgrott.gwsrobogherk;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

// TODO: Auto-generated Javadoc
/**
 * The Class ArgumentFinder.
 */
public class ArgumentFinder {

    /** The pattern. */
    private final Pattern pattern;

    /**
     * Instantiates a new argument finder.
     *
     * @param delimiter the delimiter
     */
    public ArgumentFinder(String delimiter) {
        pattern = Pattern.compile(delimiter + ".+" + delimiter);
    }

    /**
     * Find argument.
     *
     * @param string the string
     * @return the string
     */
    public String findArgument(String string) {
        List<String> arguments = new ArrayList<String>();

        Matcher matcher = pattern.matcher(string);

        while (matcher.find()) {
            arguments.add(removeOutterQuotes(new StringBuilder(matcher.group())));
        }

        return arguments.size() > 0 ? arguments.get(0) : "";
    }

    /**
     * Removes the outter quotes.
     *
     * @param match the match
     * @return the string
     */
    private String removeOutterQuotes(StringBuilder match) {
        return match.deleteCharAt(0).deleteCharAt(match.length() - 1).toString();
    }
}