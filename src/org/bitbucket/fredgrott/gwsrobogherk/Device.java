package org.bitbucket.fredgrott.gwsrobogherk;

import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertTrue;
import static junit.framework.Assert.fail;
import android.app.Activity;
import android.app.KeyguardManager;
import android.os.SystemClock;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.jayway.android.robotium.solo.Solo;

// TODO: Auto-generated Javadoc
/**
 * The Class Device.
 */
public class Device {

    /** The Constant MAX_TRIES. */
    private static final int MAX_TRIES = 5;
    
    /** The Constant TIME_TO_WAIT. */
    private static final int TIME_TO_WAIT = 125000;

    /** The solo. */
    private Solo solo;

    /**
     * Instantiates a new device.
     *
     * @param solo the solo
     */
    public Device(Solo solo) {
        this.solo = solo;
    }

    /**
     * Click and wait for.
     *
     * @param textToClick the text to click
     * @param activityToWaitFor the activity to wait for
     */
    public void clickAndWaitFor(String textToClick, Class<? extends Activity> activityToWaitFor) {
        click(textToClick);
        waitFor(activityToWaitFor);
    }

    /**
     * Click.
     *
     * @param textToClick the text to click
     */
    public void click(final String textToClick) {
        assertTrue("failed waiting for '" + textToClick + "'", solo.waitForText(textToClick, 1, TIME_TO_WAIT));
        solo.clickOnText(textToClick);
    }
    
    /**
     * Scroll to top.
     */
    public void scrollToTop() {
        while(solo.scrollUp());
    }
    
    /**
     * Scroll to bottom.
     */
    public void scrollToBottom() {
        while(solo.scrollDown());
    }
    
    /**
     * Go back.
     */
    public void goBack() {
        solo.goBack();
    }

    /**
     * Checks if is on.
     *
     * @param activity the activity
     * @return true, if is on
     */
    public boolean isOn(Class<? extends Activity> activity) {
        return solo.getCurrentActivity().getClass().getName().equals(activity.getName());
    }

    /**
     * Wait for.
     *
     * @param activityClass the activity class
     */
    public void waitFor(final Class<? extends Activity> activityClass) {
        tryToDo(new It() {
            public boolean shouldBeDone() {
                return solo.waitForActivity(activityClass.getSimpleName(), TIME_TO_WAIT);
            }
        }, activityClass.getSimpleName() + " did not appear. \nThe activity: " + solo.getCurrentActivity()
                .getClass().getSimpleName() + " was displayed while waiting.");
    }

    /**
     * Wait until not on.
     *
     * @param activity the activity
     */
    public void waitUntilNotOn(Class<? extends Activity> activity) {
        long endTime = SystemClock.currentThreadTimeMillis() + 5000;
        while (endTime > SystemClock.currentThreadTimeMillis()) {
            if (!isOn(activity))
                return;
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        fail("timed out waiting to leave " + activity.getSimpleName());
    }
    
    /**
     * Wait for.
     *
     * @param text the text
     */
    public void waitFor(final String text) {
        tryToDo(new It() {
            public boolean shouldBeDone() {
                return solo.waitForText(text, 1, TIME_TO_WAIT);
            }
        }, "failed to find text '" + text + "'");
    }

    /**
     * Wait for dialog to close.
     */
    public void waitForDialogToClose() {
        tryToDo(new It() {
            public boolean shouldBeDone() {
                return solo.waitForDialogToClose(TIME_TO_WAIT);
            }
        }, "timed out waiting for dialog to close");
    }

    /**
     * Type into field.
     *
     * @param id the id
     * @param text the text
     */
    public void typeIntoField(int id, String text) {
        final View view = solo.getView(id);
        assertNotNull("view with id " + id + " not found", view);
        assertTrue("view with id " + id + " is not an EditText (it's a " + view.getClass() + ")", EditText.class
                .isAssignableFrom(view.getClass()));
        solo.enterText((EditText) view, text);
    }

    /**
     * Assert text is visible.
     *
     * @param oneOrMoreTexts the one or more texts
     */
    public void assertTextIsVisible(final String... oneOrMoreTexts) {
        for (String text : oneOrMoreTexts){
            solo.searchText(text);
        }
    }

    /**
     * View with id is visible.
     *
     * @param id the id
     */
    public void viewWithIdIsVisible(int id) {
        assertNotNull("unable to find view! ", solo.getView(id));
    }

    /**
     * Try to do.
     *
     * @param it the it
     * @param failureMessage the failure message
     */
    public void tryToDo(It it, String failureMessage) {
        boolean isDone = false;
        for (int count = 0; !isDone && count < MAX_TRIES; count++) {
            isDone = it.shouldBeDone();
        }
        assertTrue(failureMessage, isDone);
    }

    /**
     * The Interface It.
     */
    interface It {
        
        /**
         * Should be done.
         *
         * @return true, if successful
         */
        boolean shouldBeDone();
    }

    /**
     * Click on view with id.
     *
     * @param id the id
     */
    public void clickOnViewWithId(int id) {
        View view = solo.getView(id);
        assertNotNull("view not found!", view);
        solo.clickOnView(view);
    }

    /**
     * Rotate to landscape.
     */
    public void rotateToLandscape() {
        solo.setActivityOrientation(Solo.LANDSCAPE);
    }

    /**
     * Update edit text field.
     *
     * @param editTextId the edit text id
     * @param text the text
     */
    public void updateEditTextField(int editTextId, String text) {
        solo.clearEditText((EditText) solo.getView(editTextId));
        typeIntoField(editTextId, text);
    }
    
    /**
     * Gets the text from text view.
     *
     * @param textViewId the text view id
     * @return the text from text view
     */
    public String getTextFromTextView(int textViewId) {
       View view = solo.getView(textViewId);
       assertTrue("The view requested is not a TextView!",view instanceof TextView);
       return ((TextView)view).getText().toString(); 
    }

    /**
     * Unlock screen.
     */
    @SuppressWarnings("deprecation")
    public void unlockScreen() {
        KeyguardManager manager = (KeyguardManager) solo.getCurrentActivity().getSystemService(Activity.KEYGUARD_SERVICE);
        manager.newKeyguardLock(solo.getCurrentActivity().getClass().getName()).disableKeyguard();
    }
}