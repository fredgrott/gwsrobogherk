package org.bitbucket.fredgrott.gwsrobogherk;

// TODO: Auto-generated Javadoc
/**
 * The Class StepFinder.
 */
public class StepFinder {

    /** The step class loader. */
    private final StepClassLoader stepClassLoader;

    /**
     * Instantiates a new step finder.
     *
     * @param stepClassLoader the step class loader
     */
    public StepFinder(StepClassLoader stepClassLoader) {
        this.stepClassLoader = stepClassLoader;
    }

    /**
     * Find steps for.
     *
     * @param testCaseClass the test case class
     * @return the step definitions
     */
    public StepDefinitions findStepsFor(Class<? extends Feature> testCaseClass) {
        String stepDefinitionClassName = testCaseClass.getSimpleName().concat("Steps");
        return loadStepsClass(testCaseClass.getSimpleName(), testCaseClass.getPackage().getName() + "." + stepDefinitionClassName);
    }
    
    /**
     * Load steps class.
     *
     * @param feature the feature
     * @param className the class name
     * @return the step definitions
     */
    private StepDefinitions loadStepsClass(String feature, String className) {
        try {
            Class<?> stepClass = stepClassLoader.loadClass(className);
            return (StepDefinitions) stepClass.newInstance();
        } catch (Exception e) {
            throw new NoStepsFoundException(feature, e);
        }
    }
    
}