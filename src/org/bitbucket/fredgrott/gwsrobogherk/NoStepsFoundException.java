package org.bitbucket.fredgrott.gwsrobogherk;

// TODO: Auto-generated Javadoc
/**
 * The Class NoStepsFoundException.
 */
public class NoStepsFoundException extends RoboGherkException {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;
    
    /** The feature. */
    private final String feature;
    
    /** The class name. */
    private String className;
    
    /** The exception. */
    private Exception exception;

    /**
     * Instantiates a new no steps found exception.
     *
     * @param feature the feature
     * @param exception the exception
     */
    public NoStepsFoundException(String feature, Exception exception) {
        this.feature = feature;
        this.exception = exception;
    }

    /* (non-Javadoc)
     * @see java.lang.Throwable#getMessage()
     */
    @Override
    public String getMessage() {
        return makeMessage();
    }

    /**
     * Make message.
     *
     * @return the string
     */
    public String makeMessage() {
        if (className != null )
            return String.format("\nSteps not found: %s\nPlease create class : %s", feature, className);

        return String.format("\nNo step found within the feature %s\n Please implement %s\n\n", feature, exception.getMessage());
    }
}