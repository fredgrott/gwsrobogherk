package org.bitbucket.fredgrott.gwsrobogherk;

// TODO: Auto-generated Javadoc
/**
 * The Interface StepClassLoader.
 */
public interface StepClassLoader {

    /**
     * Load class.
     *
     * @param className the class name
     * @return the class
     * @throws ClassNotFoundException the class not found exception
     */
    public Class<?> loadClass(String className) throws ClassNotFoundException;

}