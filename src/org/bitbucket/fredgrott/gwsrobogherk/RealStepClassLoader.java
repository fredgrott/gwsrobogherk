package org.bitbucket.fredgrott.gwsrobogherk;

// TODO: Auto-generated Javadoc
/**
 * The Class RealStepClassLoader.
 */
public class RealStepClassLoader implements StepClassLoader {

    /**
     * Load class.
     *
     * @param className the class name
     * @return the class
     * @throws ClassNotFoundException the class not found exception
     * @see org.bitbucket.fredgrott.gwsrobogherk.StepClassLoader#loadClass(java.lang.String)
     */
    public Class<?> loadClass(String className) throws ClassNotFoundException {
        return Class.forName(className);
    }

}