package org.bitbucket.fredgrott.gwsrobogherk;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import android.app.Instrumentation;

import com.jayway.android.robotium.solo.Solo;

// TODO: Auto-generated Javadoc
/**
 * The Class StepExecutor.
 */
public class StepExecutor {

    /** The step definitions. */
    private final StepDefinitions stepDefinitions;

    /**
     * Instantiates a new step executor.
     *
     * @param stepDefinitions the step definitions
     */
    public StepExecutor(StepDefinitions stepDefinitions) {
        this.stepDefinitions = stepDefinitions;
    }

    /**
     * Call.
     *
     * @param action the action
     */
    public void call(String action) {
        try {
            invoke(action);
        } catch (NoSuchMethodException e) {
            throw new NoStepsFoundException(stepDefinitions.getClass().getName(), e);
        } catch (InvocationTargetException e) {
            throw new RoboGherkException(e.getCause());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Invoke.
     *
     * @param action the action
     * @throws NoSuchMethodException the no such method exception
     * @throws IllegalAccessException the illegal access exception
     * @throws InvocationTargetException the invocation target exception
     */
    private void invoke(String action) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        String methodName = getMethodNameFrom(action);

        if (hasArguments(action)) {
            Method method = stepDefinitions.getClass().getMethod(methodName, String.class);
            method.invoke(stepDefinitions, getArgumentFrom(action));
        } else {
            Method method = stepDefinitions.getClass().getMethod(methodName);
            method.invoke(stepDefinitions);
        }
    }

    /**
     * Checks for arguments.
     *
     * @param action the action
     * @return true, if successful
     */
    private boolean hasArguments(String action) {
        return !"".equals(getArgumentFrom(action));
    }

    /**
     * Sets the up.
     *
     * @param instrumentation the instrumentation
     * @param solo the solo
     * @throws RoboGherkException the robo gherk exception
     */
    public void setUp(Instrumentation instrumentation, Solo solo) throws RoboGherkException {
        stepDefinitions.setTestDependecies(instrumentation, solo);
        call("setUpScenario");
    }

    /**
     * Gets the argument from.
     *
     * @param action the action
     * @return the argument from
     */
    private String getArgumentFrom(String action) {
        return new ArgumentFinder("'").findArgument(action);
    }

    /**
     * Gets the method name from.
     *
     * @param action the action
     * @return the method name from
     */
    private String getMethodNameFrom(String action) {
        if (hasArguments(action)) {
            action = action.replace("'" + getArgumentFrom(action) + "'", "arg");
        }

        return action.replace(" ", "_");
    }

    /**
     * Tear down.
     */
    public void tearDown() {
        call("cleanUpScenario");
    }
}